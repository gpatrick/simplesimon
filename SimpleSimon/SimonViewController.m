//
//  ECPIViewController.m
//  SimpleSimon
//
//  Created by Patrick, Gregory (Virginia Beach) on 3/7/13.
//  Copyright (c) 2013 Patrick, Gregory (Virginia Beach). All rights reserved.
//

#import "SimonViewController.h"

@interface SimonViewController ()

@end

@implementation SimonViewController

@synthesize scoreLabel = _scoreLabel;
@synthesize redButton = _redButton;
@synthesize greenButton = _greenButton;
@synthesize yellowButton = _yellowButton;
@synthesize blueButton = _blueButton;
@synthesize startGameButton = _startGameButton;
@synthesize patternArray = _patternArray;
@synthesize gameState = _gameState;
@synthesize playbackTimer = _playbackTimer;
@synthesize index = _index;

-(IBAction)colorButtonClicked:(UIButton *)sender{
    NSLog(@"The clicked button was: %@", sender.titleLabel.text);
    /*
     * Test code
     */
    
    [self setScore:[self score] + 1];
    [self addNextLight];
    [self toggleLight:[NSNumber numberWithInt:sender.tag]];
    
    /*
     * End Test Code
     */
}

-(IBAction)startGameButtonClicked:(id)sender{
    NSLog(@"Start Game Clicked");
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(self.patternArray == nil){
        self.patternArray = [[NSMutableArray alloc] initWithCapacity:20];
        for(int i = 0; i < 20; i++){
            [self addNextLight];
        }
    }
    
    [self.redButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.blueButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.yellowButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.greenButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

    

    
    if(!self.playbackTimer){
        self.playbackTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                              target:self
                                                            selector:@selector(fire:)
                                                            userInfo:nil
                                                             repeats:YES];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Scores
-(NSUInteger)score{
    return _score;
}

-(void)setScore:(NSUInteger)aScore{
    _score = aScore;
    self.scoreLabel.text = [NSString stringWithFormat:@"%d", _score];
}

#pragma mark - Patterns
-(void)addNextLight{
    NSUInteger light = arc4random() % 4;
    [self.patternArray addObject:[NSNumber numberWithUnsignedInteger:light]];
    for(NSNumber *number in self.patternArray){
        NSLog(@"patternArray:%@", number);
    }
    NSLog(@"----------------------");
}

-(void)fire:(NSTimer *)sender{
    NSLog(@"toggling on %@",[self.patternArray objectAtIndex:self.index]);
    [self toggleLight:[self.patternArray objectAtIndex:self.index]];
    
    [NSTimer scheduledTimerWithTimeInterval:0.2
                                     target:self
                                   selector:@selector(toggleOff:)
                                   userInfo:[self.patternArray objectAtIndex:self.index]
                                             repeats:NO];
    self.index++;
    if(self.index >= [self.patternArray count]){
        [self.playbackTimer invalidate];
    }
    
}

-(void)toggleOff:(id)sender{
    NSLog(@"toggling off index: %@", [((NSTimer *)sender) userInfo]);
    [self toggleLight:[((NSTimer *)sender) userInfo]];
}

-(void)toggleLight:(NSNumber *)lightID{
//    NSLog(@"toggling light");
    UIColor *currentColor;
    UIButton *currentButton;
    if([lightID integerValue] == 0){
        currentButton = self.redButton;
        currentColor = [UIColor redColor];
    }
    else if([lightID integerValue] == 1){
        currentButton = self.greenButton;
        currentColor = [UIColor greenColor];
    }
    else if([lightID integerValue] == 2){
        currentButton = self.blueButton;
        currentColor = [UIColor blueColor];
    }
    else if([lightID integerValue] == 3){
        currentButton = self.yellowButton;
        currentColor = [UIColor yellowColor];
    }
    
    if([currentButton titleColorForState:UIControlStateNormal] == [UIColor blackColor]){
        [currentButton setTitleColor:currentColor forState:UIControlStateNormal];
    }
    else{
        [currentButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
}

-(void)playPattern{
    
}










@end
