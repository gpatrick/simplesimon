//
//  ECPIViewController.h
//  SimpleSimon
//
//  Created by Patrick, Gregory (Virginia Beach) on 3/7/13.
//  Copyright (c) 2013 Patrick, Gregory (Virginia Beach). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimonViewController : UIViewController{
    NSUInteger _score;
}

@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UIButton *redButton;
@property (strong, nonatomic) IBOutlet UIButton *greenButton;
@property (strong, nonatomic) IBOutlet UIButton *yellowButton;
@property (strong, nonatomic) IBOutlet UIButton *blueButton;
@property (strong, nonatomic) IBOutlet UIButton *startGameButton;
@property (strong, nonatomic) NSMutableArray *patternArray;
@property (nonatomic) NSUInteger gameState;
@property (strong, nonatomic) NSTimer *playbackTimer;
@property (nonatomic) NSUInteger index;

-(IBAction)colorButtonClicked:(id)sender;
-(IBAction)startGameButtonClicked:(id)sender;

-(void)addNextLight;
-(void)toggleLight:(NSNumber *)lightID;
-(void)playPattern;
-(void)toggleOff:(id)sender;

@end
