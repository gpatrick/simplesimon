//
//  SimonConstants.h
//  SimpleSimon
//
//  Created by Patrick, Gregory (Virginia Beach) on 3/12/13.
//  Copyright (c) 2013 Patrick, Gregory (Virginia Beach). All rights reserved.
//

#ifndef SimpleSimon_SimonConstants_h
#define SimpleSimon_SimonConstants_h

#define kSimpleSimonPlaying 0
#define kSimpleSimonNotPlaying 1
#define kSimpleSimonPaused 2

#endif
