//
//  main.m
//  SimpleSimon
//
//  Created by Patrick, Gregory (Virginia Beach) on 3/7/13.
//  Copyright (c) 2013 Patrick, Gregory (Virginia Beach). All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ECPIAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ECPIAppDelegate class]));
    }
}
