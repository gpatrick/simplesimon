//
//  ECPIAppDelegate.h
//  SimpleSimon
//
//  Created by Patrick, Gregory (Virginia Beach) on 3/7/13.
//  Copyright (c) 2013 Patrick, Gregory (Virginia Beach). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ECPIAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
